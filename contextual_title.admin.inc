<?php

/**
 * @file
 * Administration pages for Contextual Title module.
 */

/**
 * Form to hide a title on a particular path.
 */
function contextual_title_form($form, &$form_state, $path, $op) {
  $form['#path'] = $path;
  $form['#op'] = $op;

  if ($op == 'hide') {
    $message = 'Would you like to hide the title on %path?';
    $button = t('Hide');
  }
  else {
    $message = 'Would you like to show the title on %path?';
    $button = t('Show');
  }

  return confirm_form(
    $form,
    t($message, array('%path' => url($path))),
    $path,
    t($message, array('%path' => url($path))),
    $button
  );
}

/**
 * Submit handler for contextual_title_hide_form().
 */
function contextual_title_form_submit(&$form, &$form_state) {
  $hidden_titles = variable_get('contextual_title_hidden', array());

  unset($hidden_titles[$form['#path']]);

  if ($form['#op'] == 'hide') {
    $hidden_titles[$form['#path']] = TRUE;
  }

  variable_set('contextual_title_hidden', $hidden_titles);
}
